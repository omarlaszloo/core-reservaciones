# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from habitaciones.models import Habitaciones
from huespedes.models import Huespedes

# Create your models here.
class Reservaciones(models.Model):
    fecha_inicial = models.DateTimeField(default=timezone.now)
    fecha_final = models.DateTimeField()
    consto_total = models.IntegerField()

class ReservacionesIdHabitacion(models.Model):
    reservaciones_id = models.ForeignKey(Reservaciones)
    habitaciones = models.ForeignKey(Habitaciones)

class ReservacionesIdHuesped(models.Model):
    reservaciones_id = models.ForeignKey(Reservaciones)
    huespedes_id = models.ForeignKey(Huespedes)
