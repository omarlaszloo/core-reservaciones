# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from hoteles.models import Hotel

class Habitaciones(models.Model):
    piso = models.IntegerField()
    numero = models.IntegerField()
    costo_x_dia = models.IntegerField()
    capacidad = models.IntegerField()
    hotel = models.ForeignKey(Hotel, related_name='habitaciones', on_delete=models.CASCADE, null=True, blank=True)
