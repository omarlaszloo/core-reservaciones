from rest_framework import serializers
from habitaciones.models import Habitaciones

class HabitacionesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Habitaciones
        fields = ('id', 'piso', 'numero', 'costo_x_dia', 'capacidad', 'hotel')
