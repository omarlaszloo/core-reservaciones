# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import status, generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from habitaciones.serializers import HabitacionesSerializer
from habitaciones.models import Habitaciones


# Create your views here.
#@api_view(['GET', 'POST'])
#def habitaciones_list(request):

#    if request.method == 'GET':
#        snippetsHabitaciones = Habitaciones.objects.all()
#        serializer = HabitacionesSerializer(snippetsHabitaciones, many=True)
#        return Response(serializer.data)

#    if request.method == 'POST':
#        print 'request.data', request.data
#        serializer = HabitacionesSerializer(data=request.data)
#        print 'serializer', serializer
#        if serializer.is_valid():
#            serializer.save()
#            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
#        return JsonResponse(serializer.error, status=status.HTTP_400_BAD_REQUEST)


class habitaciones_list(generics.ListCreateAPIView):
    queryset = Habitaciones.objects.all()
    serializer_class = HabitacionesSerializer
