
from django.conf.urls import url
from habitaciones.views import habitaciones_list

urlpatterns = [
    url(r'^add-habitaciones/$', habitaciones_list.as_view())
]
