
###### instalacion y configuracion del ambiente #######


############################
   config docker - image
   config django
############################

para construir el build del proyecto debemos de seguir los siguientes pasos

paso 1

$  sudo docker-compose up // con esto levantará e instalará todas nuestras dependencias.


paso 2

debemos de crearnos un super-user , esto lo creamos ingresando al core de la imagen,
lo hacemos con el siguiente comando

  $ sudo docker ps -a // para obtener el id del proceso de la imagen

despues ejecutamos el siguiente comando.

  $ sudo docker exec -it <uid-container> bash

paso 3

una vez que estemos dentro de la imagen , procedemos a crear nuestro super admin.
lo hacemos de la siguiente forma

$ python manager.py createsuperuser // esto nos creará el user

**** en el proceso nos pedira nuestros datos de email , user y pass ****

Nota  """ antes de crearnos el super-user debemos de hacer un migrate para que se creeen las tablas correspondientes """


############################
   config postgres
############################

paso 1
  para acceder al contenedor de postgres ingresamos el siguiente comando

  $ sudo docker exec -it < uid container > bash

  una vez dentro del contenedor ingresamos a psql con el siguiente comando

  # psql -U postgres

  o bien podemos acceder esta forma

  $ sudo docker exec -it <uid del container> psql -U postgres

  para ver todas las tables

  #  \d

paso 2
para cambiar nuestro user y pass.

  ALTER USER postgres WITH ENCRYPTED PASSWORD '12345';


  ############################
     creacion de modulos
  ############################

  para crear modulos debemos de ingresar al container

  $ sudo docker exec -it <uid container> bash

  # python manage.py startapp app
