# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from huespedes.models import Huespedes
from habitaciones.models import Habitaciones

class Ocupantes(models.Model):
    id_habitaciones = models.ForeignKey(Habitaciones)
    id_habitacion = models.ForeignKey(Huespedes)
