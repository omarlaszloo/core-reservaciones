# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

class Huespedes(models.Model):
    nombre = models.CharField(max_length=100)
    apellido_materno = models.CharField(max_length=100)
    apellido_paterno = models.CharField(max_length=100)

    def __unicode__ (self):
        return self.nombre
