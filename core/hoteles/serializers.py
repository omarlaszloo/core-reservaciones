from rest_framework import serializers
from hoteles.models import Hotel
from habitaciones.models import Habitaciones
from habitaciones.serializers import HabitacionesSerializer

class HotelSerializer(serializers.ModelSerializer):

    habitaciones = HabitacionesSerializer(many=True)

    class Meta:
        model = Hotel
        fields = ('id', 'name', 'description', 'calif', 'habitaciones')

    def create(self, validated_data):
        albums_data = validated_data.pop('habitaciones')
        musician = Hotel.objects.create(**validated_data)
        for album_data in albums_data:
            Habitaciones.objects.create(Habitaciones=musician, **album_data)
        return musician
