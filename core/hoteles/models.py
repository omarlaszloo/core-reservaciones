# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Hotel(models.Model):
    #id = models.AutoField(primary_key=True)
    name= models.CharField("name", max_length=50)
    description = models.CharField("description", max_length=50)
    calif = models.CharField("calif", max_length=50)

    def __unicode__ (self):
        return self.name
