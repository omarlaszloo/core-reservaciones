
from django.conf.urls import url
from hoteles.views import hotel_list

urlpatterns = [
    url(r'^add-hotel/$', hotel_list.as_view())
]
