# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import status, generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from hoteles.serializers import HotelSerializer
from hoteles.models import Hotel


# Create your views here.
# @api_view(['GET', 'POST'])
# def hotel_list(request):

#     if request.method == 'GET':
#         snippetsHotel = Hotel.objects.all()
#         serializer = HotelSerializer(snippetsHotel, many=True)
#         return Response(serializer.data)

#     if request.method == 'POST':

#         serializer = HotelSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
#         return JsonResponse(serializer.error, status=status.HTTP_400_BAD_REQUEST)


class hotel_list(generics.ListCreateAPIView):
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializer
